
<?php 
	require('FuelSDK-PHP-Dot9/ET_Client.php');
	try {

		$myclient = new ET_Client(true);

		$subscriber = new ET_Subscriber();
		$subscriber->authStub = $myclient;
		$subscriber->props = array("ID","EmailAddress");
		$subscriber_details = $subscriber->get();
		$suscriber_email = array();
		
		if(!empty($subscriber_details->results))
		{
			foreach($subscriber_details->results as $subscriber_detail)
			{
				$suscriber_email[$subscriber_detail->ID] = $subscriber_detail->EmailAddress;
			}
		}

		$getList = new ET_List();
		$getList->authStub = $myclient;
		$getList->props = array("ID","ListName");
		$responses_lists = $getList->get();

		$lists_array = array();
		if(!empty($responses_lists->results))
		{
			foreach($responses_lists->results as $response)
			{
				$lists_array[$response->ID] = $response->ListName;
			}
		}

		//getting suscribers list
		$listsubscriber = new ET_List_Subscriber();
		$listsubscriber->authStub = $myclient;
		$responses = $listsubscriber->get();

		$suscribers_array = array();
		if(!empty($responses->results))
		{
			foreach($responses->results as $response)
			{
				$suscribers_array[] = array('et_client_id'=>$response->Client->ID,
											'et_susciber_id'=>$response->ID,
											'susciber_email'=>isset($suscriber_email[$response->ID])?$suscriber_email[$response->ID]:'',
											'suscriber_key'=>$response->SubscriberKey,
											'list_name'=>isset($lists_array[$response->ListID])?$lists_array[$response->ListID]:'',
											'Status'=>$response->Status,
											'createdate'=>$response->CreatedDate,
											'modifieddate'=>$response->ModifiedDate);
			}
		}
	}
	catch (Exception $e) {
		$url = admin_url( 'admin.php?page=et-setting' );
		$errors = explode('.',$e->getMessage());
		echo '<div class="updated settings-error"><p><strong>'.$errors[0].'.</strong></p><a href="'.$url.'">Click here to add or update setting. </a></div>';
	}

?>

<div class="wrap">
	<h2>Manage Suscribers</h2>

	<table id="suscriber_table">
		<thead>
			<tr>
				<th>Client ID</th>
				<th>Suscriber ID</th>
				<th>Email</th>
				<th>Key</th>
				<th>List Name</th>
				<th>Status</th>
				<th>Created Date</th>
				<th>Modified Date</th>
			</tr>
		</thead>
		<tbody>
			<?php if(!empty($suscribers_array)): foreach($suscribers_array as $suscriber): ?>
			<tr>
				<td><?php echo $suscriber['et_client_id']; ?></td>
				<td><?php echo $suscriber['et_susciber_id']; ?></td>
				<td><?php echo $suscriber['susciber_email']; ?></td>
				<td><?php echo $suscriber['suscriber_key']; ?></td>
				<td><?php echo $suscriber['list_name']; ?></td>
				<td><?php echo $suscriber['Status']; ?></td>
				<td><?php echo $suscriber['createdate']; ?></td>
				<td><?php echo $suscriber['modifieddate']; ?></td>
			</tr>
			<?php endforeach; endif; ?>
		</tbody>	
	</table>
</div>

<script>
jQuery(function($){
	$('#suscriber_table').DataTable();
})
</script>