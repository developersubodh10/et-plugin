<?php
    if($_POST['ano_hidden'] == 'Y') :

    	$posted_datas = $_POST;
    	unset($posted_datas['ano_hidden']);
    	unset($posted_datas['Submit']);

    	$et_client_id    = sanitize_text_field( $posted_datas['et_client_id'] );
        $et_client_secrete_key   = sanitize_text_field( $posted_datas['et_client_secrete_key'] );
        $et_client_appsignature = sanitize_text_field( $posted_datas['et_client_appsignature'] );
        $et_wsdl = sanitize_text_field( $posted_datas['et_wsdl'] );

        $errors = array();

        if(!empty($et_client_id) && strlen($et_client_id) != 24)
        	$errors['error_et_client_id'] = '<label class="et_error" style="color:red;">Client ID Required and must be 24 characters.</label>';

        if(!empty($et_client_secrete_key) && strlen($et_client_secrete_key) != 24)
        	$errors['error_et_client_secrete_key'] = '<label class="et_error" style="color:red;">Client Secerete Key Required  and must be 24 characters.</label>';

        if (!empty($et_wsdl) && !preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$et_wsdl))
            $errors['error_et_wsdl'] = '<label class="et_error" style="color:red;">Valid wsdl Required. </label>';

        $data_to_save = array(	'et_client_id'	=>	$et_client_id,
								'et_client_secrete_key'	=>	$et_client_secrete_key,
								'et_client_appsignature'	=>	$et_client_appsignature,
								'et_wsdl'	=>	$et_wsdl
							);

        if(empty($errors))
        {
        	update_option('et_options', $data_to_save); ?>

        	<div class="updated"><p><strong><?php _e('Options saved.' ); ?></strong></p></div>
        
        <?php   }

        $et_options = $data_to_save;

    else : 
     	$et_options = get_option('et_options');
    endif; ?>

<div class="wrap">
	<?php  echo "<h2>" . __( 'Exact Target Options', 'oscimp_trdom' ) . "</h2>"; ?>
	 
	<form name="et_option_form" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">

	    <input type="hidden" name="ano_hidden" value="Y">

	    <table class="form-table">
            <tr valign="top">
                <th scope="row">
                    <label for="num_elements">
                    	<?php    echo __( 'Client ID ', 'oscimp_trdom' ); ?>
                    </label> 
                </th>
                <td>
                    <input type="text" name="et_client_id" size="24" value="<?php echo $et_options['et_client_id']; ?>"/>
					<?php if(isset($errors['error_et_client_id'])) echo $errors['error_et_client_id']; ?>
                    <p>Note : 24 characters </p>
                </td>
            </tr>

            <tr valign="top">
                <th scope="row">
                    <label for="num_elements">
                    	<?php    echo __( 'Client Secete Key ', 'oscimp_trdom' ); ?>
                    </label> 
                </th>
                <td>
                    <input type="text" name="et_client_secrete_key" size="24" value="<?php echo $et_options['et_client_secrete_key']; ?>"/>
                    <?php if(isset($errors['error_et_client_secrete_key'])) echo $errors['error_et_client_secrete_key']; ?>
                    <p>Note : 24 characters </p>
                </td>
            </tr>

			<tr valign="top">
                <th scope="row">
                    <label for="num_elements">
                    	<?php    echo __( 'WSDL ', 'oscimp_trdom' ); ?>
                    </label> 
                </th>
                <td>
                    <input type="text" name="et_wsdl" size="50" value="<?php echo $et_options['et_wsdl']; ?>"/>
                    <?php if(isset($errors['error_et_wsdl'])) echo $errors['error_et_wsdl']; ?>
                    <p>Defalut wsdl : https://webservice.exacttarget.com/etframework.wsdl </p>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">
                    <label for="num_elements">
                    	<?php    echo __( 'Client App Signature', 'oscimp_trdom' ); ?>
                    </label> 
                </th>
                <td>
                    <input type="text" name="et_client_appsignature" size="36" value="<?php echo $et_options['et_client_appsignature']; ?>"/>
                </td>
            </tr>
        </table>

		<p class="submit">
	    <input type="submit" name="Submit" value="<?php _e('Update Options', 'oscimp_trdom' ) ?>" />
	    </p>

	</form>
    <div>
        <h2>About </h2>
        <p>This is simple plugin for integrating exact target email api with wordpress.Client ID and Client Secete Key are needed to conncet with your exact target account.Please insert wsdl according to your instance.<a href="http://help.exacttarget.com/en/documentation/exacttarget/resources/accessing_the_exacttarget_application/">more for instance</a> </p>

         <h2>Usage </h2>
        <p>This plugin is simple to use.Put shortcut [et-form] where you want to display the suscription form.The form will contain name and email.</p>
    </div>
</div>

<script>
	jQuery(document).ready(function($){

		$('#add_new_book').click(function(){

			var count = $("div#input_div_block input").not('.remove_book').length;
			count++;
			var html =  '<p>Book '+count+': <input type="text" name="ano_book'+count+'" value="" size="20"><input type="button" value="Remove" class="remove_book"></p>';
			$('#input_div_block').append(html);
		})

		$(document).on('click',".remove_book",function(){
			$(this).parent('p').remove();
		})
	})
</script>