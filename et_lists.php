
<?php 
	require('FuelSDK-PHP-Dot9/ET_Client.php');

	try {

		$myclient = new ET_Client(true);

		$listsubscriber = new ET_List_Subscriber();
		$listsubscriber->authStub = $myclient;
		$listsubscriber->props = array("ListID","ID");
		$responses = $listsubscriber->get();

		$list_sus_counts = array();
		$new_list_count = array();
		if(!empty($responses->results))
		{
			foreach($responses->results as $result)
			{
				$list_sus_counts[$result->ListID][] = $result->ID;
			}
			if(!empty($list_sus_counts))
			{
				foreach($list_sus_counts as $k=>$list_sus_count)
				{
					$new_list_count[$k] = count($list_sus_count);
				}
			}
		}

		//getting list 
		$getList = new ET_List();
		$getList->authStub = $myclient;
		$getList->props = array("ID","PartnerKey","CreatedDate","ModifiedDate","Client.ID","Client.PartnerClientKey","ListName","Description","Category","Type","CustomerKey","ListClassification","AutomatedEmail.ID");
		$responses = $getList->get();

		$lists_array = array();
		if(!empty($responses->results))
		{
			foreach($responses->results as $response)
			{
				$lists_array[] = array(		'et_client_id'=>$response->Client->ID,
											'et_list_id'=>$response->ID,
											'name'=>$response->ListName,
											'type'=>$response->Type,
											'description'=>$response->Description,
											'classification'=>$response->ListClassification,
											'createdate'=>$response->CreatedDate,
											'modifieddate'=>$response->ModifiedDate);
			}
		}

		$active_list = get_option('et_active_list');
	}
	catch (Exception $e) {
		$url = admin_url( 'admin.php?page=et-setting' );
		$errors = explode('.',$e->getMessage());
		echo '<div class="updated settings-error"><p><strong>'.$errors[0].'.</strong></p><a href="'.$url.'">Click here to add or update setting. </a></div>';
	}
?>

<div class="wrap">
	<h2>Manage List</h2>

	<table id="list_table">
		<thead>
			<tr>
				<th>Client ID</th>
				<th>List ID</th>
				<th>Name</th>
				<th>Type</th>
				<th>Classification</th>
				<th>Suscriber No.</th>
				<th>Created Date</th>
				<th>Modified Date</th>
				<th>Active</th>
			</tr>
		</thead>
		<tbody>
			<?php if(!empty($lists_array)): foreach($lists_array as $list): ?>
			<tr>
				<td><?php echo $list['et_client_id']; ?></td>
				<td><?php echo $list['et_list_id']; ?></td>
				<td><?php echo $list['name']; ?></td>
				<td><?php echo $list['type']; ?></td>
				<td><?php echo $list['classification']; ?></td>
				<td><?php echo isset($new_list_count[$list['et_list_id']])?$new_list_count[$list['et_list_id']]:0 ; ?></td>
				<td><?php echo $list['createdate']; ?></td>
				<td><?php echo $list['modifieddate']; ?></td>
				<td><input type="radio" name="active_list" class="active_list" list_id="<?php echo $list['et_list_id']; ?>" <?php echo ($active_list == $list['et_list_id'])?"checked='checked'": ""; ?>></td>
			</tr>
			<?php endforeach; endif; ?>
		</tbody>	
	</table>
</div>

<script>

jQuery(function($){

	$('#list_table').DataTable();

	$('.active_list').click(function(){

		var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
		list_id = $(this).attr('list_id');

		$('.settings-error').remove();
		$.post (
		    ajaxurl, 
		    {
		        'action': 'activate_list',
		        'list_id':   list_id
		    }, 
		    function(response){
		    	if(response == true)
		        	$('.wrap h2').after('<div class="updated settings-error" id="setting-error-settings_updated"><p><strong>Settings saved.</strong></p></div>');
		        else
		        	$('.wrap h2').after('<div class="updated settings-error" id="setting-error-settings_updated"><p><strong>Something Went Wrong.</strong></p></div>');
		    }
		);
	})
});
</script>