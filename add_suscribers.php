<form method="POST" id="suscriber_form">
  Name: <input type="text" name="fullname" id="et_sus_fullname"><br>
  Email: <input type="email" name="email" id="et_sus_email"><br>
  <input type="submit" name="submit" value="Sign up"><br>
</form>

<script>

jQuery(function($){

	$('#suscriber_form').submit(function(e){

		e.preventDefault();
		var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";

		var fullname = $('#et_sus_fullname').val();
		var email = $('#et_sus_email').val();

		$('.et_status_message').remove();
		$.ajax({
          type:'POST',
          dataType:'json',
          data:{action:'add_suscribers','fullname' : fullname,'email' : email,'submit' : 'Sign up'},
          url: ajaxurl,
          success: function(value) {

	          	if(value.status == false)
	          	{
	          		$('#et_sus_email').after('<p class="et_status_message">'+value.results[0].StatusMessage+'</p>');
	          	}
	          	else
	          	{
	          		$('#et_sus_email').after('<p class="et_status_message">You are now our suscriber.Thank you for suscribing.</p>');
	          	}
          }
        });
	})
});
</script>