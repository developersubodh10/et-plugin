<?php
/**
 * @package Exact target
 * Plugin Name: Exact Target
 * Description: Custom Plugin made for wordpress project to integrate Exact target API.
 * Version: 1.0.1
 * Author: Subodh Dhukuchhu
 * Author URI: subodhdhukuchhu.com.np
*/

  if ( ! defined( 'ABSPATH' ) ) {
      exit;
  }

  function css_and_js() {
    
    wp_register_style('datatable_css', plugins_url('css/jquery.dataTables.min.css',__FILE__ ));
    wp_enqueue_style('datatable_css');
    wp_register_script( 'datatable_js', plugins_url('js/jquery.dataTables.min.js',__FILE__ ));
    wp_enqueue_script('datatable_js');

    wp_register_style('jqueryui_css', plugins_url('css/jquery-ui.min.css',__FILE__ ));
    wp_enqueue_style('jqueryui_css');
    wp_register_script( 'jqueryui_js', plugins_url('js/jquery-ui.min.js',__FILE__ ));
    wp_enqueue_script('jqueryui_js');
  }

  add_action( 'admin_init','css_and_js');

  add_action( 'wp_ajax_activate_list', 'activate_list' );

  function activate_list() {
      $list_id = trim($_POST['list_id']);
      $status = update_option('et_active_list', $list_id);
  }

  add_action( 'wp_ajax_add_suscribers', 'add_suscribers' );

  function add_suscribers() {

      if($_POST['submit'] == 'Sign up')
      {
          $name = $_POST['fullname'];
          $email = $_POST['email'];
          $list_id = get_option('et_active_list');
          $suscriber_key = 'SDK_'.md5(mktime());

          require('FuelSDK-PHP-Dot9/ET_Client.php');
          $myclient = new ET_Client(true);
          $subscriber = new ET_Subscriber();
          $subscriber->authStub = $myclient;
          $subscriber->props = array("EmailAddress" => $email, "SubscriberKey" => $suscriber_key, "Lists" => array("ID" => $list_id));
          $subscriber->props['Attributes'] = array(array('Name' => 'Name', 'Value' => $name));
          $results = $subscriber->post();

          echo json_encode($results);
          die();
      }
  }

  register_activation_hook( __FILE__, 'jal_install' );

  global $jal_db_version;
  $jal_db_version = '1.0';

  function jal_install () {
    global $wpdb;
    global $jal_db_version;

    $suscriber = $wpdb->prefix . "et_suscriber"; 
    $suscriber_meta = $wpdb->prefix . "et_suscriber_meta"; 

    global $wpdb;

    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE $suscriber (
      id mediumint(9) NOT NULL AUTO_INCREMENT,
        et_client_id mediumint(9) NOT NULL,
        et_susciber_id mediumint(9) NOT NULL,
      susciber_email varchar(20) DEFAULT '' NOT NULL,
      suscriber_key varchar(20) DEFAULT '' NOT NULL,
        list_id mediumint(9) NOT NULL,
        Status varchar(20) DEFAULT '' NOT NULL,
      createdate datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        modifieddate datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
      UNIQUE KEY id (id)
    ) $charset_collate;";

    $sql1 = "CREATE TABLE $suscriber_meta (
      id mediumint(9) NOT NULL AUTO_INCREMENT,
      suscriber_id varchar(20) DEFAULT '' NOT NULL,
      name varchar(20) DEFAULT '' NOT NULL,
      value text NOT NULL,
      UNIQUE KEY id (id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
    dbDelta( $sql1 );
    add_option( 'jal_db_version', $jal_db_version );
  }

  add_action( 'admin_menu', 'register_my_custom_menu_page' );

  function register_my_custom_menu_page(){
      add_menu_page('Exact Target', 'Exact Target', 'manage_options', 'custompage');
      add_submenu_page( 'custompage', 'Options', 'Options', 'manage_options', 'et-setting','et_setting');
      add_submenu_page( 'custompage', 'Suscribers', 'Suscribers', 'manage_options', 'et-suscribers','suscriber_page');
      add_submenu_page( 'custompage', 'List', 'List', 'manage_options', 'et-list','list_page');
      remove_submenu_page( 'custompage', 'custompage' );
  }

  function et_setting(){
    include('et_option.php');
  }

  function suscriber_page(){
    include('et_suscribers.php');
  }

  function list_page(){
      include('et_lists.php');
  }

  function et_suscription_form(){
    include('add_suscribers.php');
  }
  add_shortcode('et-form', 'et_suscription_form');

